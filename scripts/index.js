// Теоретичні питання

//     1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.

//          Екранування здійснюється зворотним слешем та використовується для позначення класів символів, наприклад d. Це спеціальний символ у регулярних виразах. Також для виведення в кавичках кавичок та інших символів.

//     2. Які засоби оголошення функцій ви знаєте?

//          Function expretion, function declaration, array function

//     3. Що таке hoisting, як він працює для змінних та функцій?

//          Для function declaration hoisting працює при оголошенні функції в будь-якому місці коду. Для Function expretion та змінних потрібно спочатку оголосити функції, змінну і вже тоді до них звертатися.

// Завдання

let number1Hint = document.querySelector('.number1-hint');
let number2Hint = document.querySelector('.number2-hint');

const errorHandler = (text, place) => {
    if (place === 'firstName') {
        number1Hint.innerText = text;
    } else if (place === 'secondName') {
        number2Hint.innerText = text;
    }
};

const action1 = document.querySelector('.input-num1');
const action2 = document.querySelector('.input-num2');

action1.addEventListener('input', e => {
    e.target.value = e.target.value.replace(/\d/g, '');
});
action2.addEventListener('input', e => {
    e.target.value = e.target.value.replace(/\d/g, '');
});

const validateNumber = (firstName, secondName) => {
    let switcher = true;

    if (!firstName) {
        errorHandler("Ви не ввели ім'я!", 'firstName');

        switcher = false;
    }

    if (!secondName) {
        errorHandler('Ви не ввели фамілію!', 'secondName');

        switcher = false;
    }

    if (!!firstName && Number(firstName)) {
        errorHandler("Ви ввели некоректне ім'я!", 'firstName');

        switcher = false;
    }

    if (!!secondName && Number(secondName)) {
        errorHandler('Ви ввели некоректну фамілію!', 'secondName');

        switcher = false;
    }

    return switcher;
};

const getAction = () => {
    const firstName = document.querySelector('.input-num1');
    const secondName = document.querySelector('.input-num2');
    const date = document.querySelector('.input-date');
    const output = document.querySelector('.output');

    number1Hint.innerText = '';
    number2Hint.innerText = '';
    output.innerText = '';

    if (validateNumber(firstName.value, secondName.value)) {
        const user = createNewUser(firstName.value, secondName.value, date.value);

        output.innerText = 'Login: ' + user.getLogin() + ', вік: ' + user.getAge() + ' років, pwd: ' + user.getPassword();
        console.log('Login: ' + user.getLogin() + ', вік: ' + user.getAge() + ' років, pwd: ' + user.getPassword());
    }
};

const createNewUser = (firstName = 'Empty', secondName = 'Empty', date = 'Empty') => {
    const newUser = {
        _firstName: firstName,
        _secondName: secondName,
        _birthDate: new Date(date),

        get firstName() {
            return this._firstName;
        },

        get secondName() {
            return this._secondName;
        },

        set firstName(name) {
            this._firstName = name;
        },

        set secondName(name) {
            this._secondName = name;
        },

        getLogin() {
            return (this._firstName.trim()[0] + this._secondName).toLowerCase();
        },

        getAge() {
            const nowDate = new Date();
            const nowYear = nowDate.getFullYear();
            const birthDateYear = this._birthDate.getFullYear();

            return nowYear - birthDateYear;
        },

        getPassword() {
            const birthDateYear = this._birthDate.getFullYear();

            return this._firstName[0].toUpperCase() + this._secondName.toLowerCase() + birthDateYear;
        },
    };

    return newUser;
};

const onSubmit = () => {
    const submitBtn = document.querySelector('.submit');

    submitBtn.addEventListener('click', getAction);
};

onSubmit();
